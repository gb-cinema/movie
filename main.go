package main

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
)

var Addr = ":8080"

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/movies", movieListHandler).Methods("GET")
	r.HandleFunc("/movies/{id}", movieGetHandler).Methods("GET")

	log.Printf("Starting on port %s", Addr)
	log.Fatal(http.ListenAndServe(Addr, r))
}

type Movie struct {
	ID           int       `json:"id"`
	Name         string    `json:"name"`
	OriginalName string    `json:"original_name"`
	Poster       string    `json:"poster"`
	Plot         string    `json:"plot"`
	MovieUrl     string    `json:"movie_url"`
	IsPaid       bool      `json:"is_paid"`
	ReleaseDate  time.Time `json:"release_date"`
	Genre        string    `json:"genre"`
}

func timeMustParse(year string) time.Time {
	t, err := time.Parse("2006", year)
	if err != nil {
		panic(err)
	}
	return t
}

func movieListHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	mm := []Movie{
		{1, "Бойцовский клуб", "Fight Club", "/static/posters/fightclub.jpg", "Терзаемый хронической бессонницей и отчаянно пытающийся вырваться из мучительно скучной жизни клерк встречает некоего Тайлера Дардена, харизматического торговца мылом с извращенной философией.", "https://youtu.be/qtRKdVHc-cE", true, timeMustParse("1999"), "thriller"},
		{2, "Крестный отец", "The Godfather", "/static/posters/father.jpg", "Криминальная сага, повествующая о нью-йоркской сицилийской мафиозной семье Корлеоне.", "https://youtu.be/ar1SHxgeZUc", false, timeMustParse("1972"), "crime"},
		{3, "Криминальное чтиво", "Pulp Fiction", "/static/posters/pulpfiction.jpg", "Двое бандитов Винсент Вега и Джулс Винфилд проводят время в философских беседах в перерыве между разборками и «решением проблем» с должниками своего криминального босса Марселласа Уоллеса. Параллельно разворачивается три истории.", "https://youtu.be/s7EdQ4FqbhY", true, timeMustParse("1994"), "drama"},
	}

	var res []Movie
	for _, movie := range mm {
		genres := r.URL.Query().Get("genres")
		if genres != "" {
			for _, genre := range strings.Split(genres, ",") {
				if movie.Genre == genre {
					res = append(res, movie)
				}
			}
		} else {
			res = mm
		}
	}

	err := json.NewEncoder(w).Encode(res)
	if err != nil {
		log.Printf("Render response error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
	return
}

func movieGetHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	vars := mux.Vars(r)
	movieID := vars["id"]

	mm := []Movie{
		{1, "Бойцовский клуб", "Fight Club", "/static/posters/fightclub.jpg", "Терзаемый хронической бессонницей и отчаянно пытающийся вырваться из мучительно скучной жизни клерк встречает некоего Тайлера Дардена, харизматического торговца мылом с извращенной философией.", "https://youtu.be/qtRKdVHc-cE", true, timeMustParse("1999"), "thriller"},
		{2, "Крестный отец", "The Godfather", "/static/posters/father.jpg", "Криминальная сага, повествующая о нью-йоркской сицилийской мафиозной семье Корлеоне.", "https://youtu.be/ar1SHxgeZUc", false, timeMustParse("1972"), "crime"},
		{3, "Криминальное чтиво", "Pulp Fiction", "/static/posters/pulpfiction.jpg", "Двое бандитов Винсент Вега и Джулс Винфилд проводят время в философских беседах в перерыве между разборками и «решением проблем» с должниками своего криминального босса Марселласа Уоллеса. Параллельно разворачивается три истории.", "https://youtu.be/s7EdQ4FqbhY", true, timeMustParse("1994"), "drama"},
	}

	var res Movie
	for _, movie := range mm {
		if strconv.Itoa(movie.ID) == movieID {
			res = movie
		}
	}
	if res.ID == 0 {
		log.Printf("Render response error: %v", errors.New("movie not found"))
		w.WriteHeader(http.StatusNotFound)
		return
	}
	err := json.NewEncoder(w).Encode(&res)
	if err != nil {
		log.Printf("Render response error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
	return
}
